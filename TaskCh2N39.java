/*
 * Created by Anton on 11.06.2017.
 *
 * Даны целые числа h, m, s (0 < h ≤ 23, 0 ≤ m ≤ 59, 0 ≤ s ≤ 59), указывающие
 * момент времени: "h часов, m минут, s секунд". Определить угол (в градусах)
 * между положением часовой стрелки в начале суток и в указанный момент времени.
 *
 */
import java.util.Scanner;
public class TaskCh2N39 {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a hours ");
        int hours = in.nextInt();
        System.out.println("Enter a minutes ");
        int minutes = in.nextInt();
        System.out.println("Enter a seconds ");
        int seconds = in.nextInt();
        int degrees;
        if (hours > 0 && hours <= 23 && minutes >= 0 && minutes <= 59 && seconds >= 0 && seconds <= 59) {
            hours = hours % 12; // за 12 часов (окружность равна 12 часам = 360°)
            seconds = hours * 3600 + minutes * 60 + seconds; // сумма секунд из часы+минуты+секунды
            degrees = 360 * seconds / 12 / 3600; // рассчет угола
            System.out.print("Result: " + degrees + " degrees");
        }
        else {
            System.out.println("You entered the wrong time");
        }
    }
}


/*
 *
 *(360 градусов / 12 часов *60 минут *60 секнд) = градусов в 1 секунде
 *
 * 12 часов - 360°
 * 1 час - 30°
 * 1 градус  2 минуты
 *
 *
 *
 *
 */