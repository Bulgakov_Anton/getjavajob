/*
 * Created by Anton on 11.06.2017.
 *
 * Записать по правилам изучаемого языка программирования следующие выражения: р)
 */
public class TaskCh1N17r {
    public static void main(String args[]){
        int input = 2;
        System.out.format("Result: %.5f", (Math.sqrt(input+1)+Math.sqrt(input-1))/(2*Math.sqrt(input)));
    }
}