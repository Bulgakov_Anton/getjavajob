/*
 * Created by Anton on 12.06.2017.
 *
 * Даны два целых числа a и b. Если a делится на b или b делится на a, то вывести 1,
 * иначе — любое другое число. Условные операторы и операторы цикла не использовать.
 *
 */
import java.util.Scanner;
public class TaskCh2N43 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a = in.nextInt();
        int b = in.nextInt();
        int c;
        int d;
        c = a % b;
        d = b % a;
        System.out.println(c*d+1);
    }
}
