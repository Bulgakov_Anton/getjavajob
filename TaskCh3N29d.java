/*
 * Created by Anton on 15.06.2017.
 *
 * Записать условие, которое является истинным, когда:
 * д) только одно из чисел X, Y и Z кратно пяти;
 *
 * don’t use * / +  - operators, in b solve with <=2 and in d - with <=3 comparison operators;
 * don't use extra parentheses!
 * write task's text as javadoc for each solution;
 * д) task cover with all possible test inputs
 *
 */

public class TaskCh3N29d {
    public static void main (String[] args) {
        // условие, когда одно из чисел равно 5
        int x = 5;
        int y = 3;
        int z = 1;
        boolean a = x % 5 == 0;
        boolean b = y % 5 == 0;
        boolean c = z % 5 == 0;
        // тип boolean, используемый для хранения логических значений.
        // Переменные этого типа могут принимать всего два значения — true (истина) и false (ложь).

        /*
         * x = 5, y = 3, z = 1;
         * a = 5 % 5 == 0, b = 3 % 5 == 0, c = 1 % 5 == 0;
         * a = 0 == 0, b = 3 != 0, c = 1 != 0;
         */
        boolean result = !( a && b && c ) && ( a ^ b ^ c );
        /*
         * result = !( 0 && 3 && 1 ) && ( 0 ^ 3 ^ 1 );
         * result = !( 0 ) && ( 2 );
         * result = 1 && 2;
         * result = 0;
         * result = true
         */
        System.out.println("result x == 5; y,z != 5 : " + result);

        // условие, когда два числа равны 5
        x = 5;
        y = 5;
        z = 1;
        a = x % 5 == 0;
        b = y % 5 == 0;
        c = z % 5 == 0;
        result = !( a && b && c ) && ( a ^ b ^ c );
        System.out.println("result x,y == 5; z != 5 : " + result);

        // условие, когда все три числа равны 5
        x = 5;
        y = 5;
        z = 5;
        a = x % 5 == 0;
        b = y % 5 == 0;
        c = z % 5 == 0;
        result = !( a && b && c ) && ( a ^ b ^ c );
        System.out.println("result x,y,z == 5 : " + result);
    }
}