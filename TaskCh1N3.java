/*
 * Created by Anton on 10.06.2017.
 *
 * Составить программу вывода на экран числа, вводимого с клавиатуры.
 * Выводимому числу должно предшествовать сообщение "Вы ввели число".
 *
 */
import java.util.Scanner;
public class TaskCh1N3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a number ");
        int input = in.nextInt();
        System.out.println("You entered a number " + input);
    }
}
