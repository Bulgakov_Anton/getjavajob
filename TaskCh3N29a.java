/*
 * Created by Anton on 15.06.2017.
 *
 * Записать условие, которое является истинным, когда:
 * а) каждое из чисел X и Y нечетное;
 *
 * don’t use * / +  - operators, in b solve with <=2 and in d - with <=3 comparison operators;
 * don't use extra parentheses!
 * write task's text as javadoc for each solution;
 * д) task cover with all possible test inputs
 *
 */

public class TaskCh3N29a {
    public static void main (String[] args) {
        //  каждое из чисел X и Y нечетное (истина)
        int x = 5;
        int y = 3;
        boolean a = x % 2 == 1;
        boolean b = y % 2 == 1 ;
        boolean result = a & b ;
        System.out.println("result : " + result);

        //  одно из чисел X и Y нечетное (ложь)
        x = 4;
        y = 3;
        a = x % 2 == 1;
        b = y % 2 == 1 ;
        result = a & b ;
        System.out.println("result : " + result);

        //  оба из числа X и Y четные (ложь)
        x = 8;
        y = 14;
        a = x % 2 == 1;
        b = y % 2 == 1 ;
        result = a & b ;
        System.out.println("result : " + result);
    }
}
