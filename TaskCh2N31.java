/*
 * Created by Anton on 11.06.2017.
 *
 * В трехзначном числе x зачеркнули его вторую цифру. Когда к образованному
 * при этом двузначному числу справа приписали вторую цифру числа x, то
 * получилось число n. По заданному n найти число x (значение n вводится с
 * клавиатуры, 100 ≤ n ≤ 999).
 */
import java.util.Scanner;
public class TaskCh2N31 {
    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a number ");
        int input = in.nextInt();
        input = 100 * (input / 100) + 10 * (input % 10) + input / 10 % 10;
        System.out.print(input);
    }
}

/*
 * Вычисляем три цифры числа:
 * input / 100 - первая цифра;
 * input / 10 % 10 - вторая цифра;
 * input % 10 - третья цифра.
 *
 *
 * input = 132
 *
 * input = 100 * (input / 100) + 10 * (input % 10) + input / 10 % 10 -->
 * input = 100 * (132 / 100) + 10 * (132 % 10) + 132 / 10 % 10 -->
 * input = 100 * (1) + 10 * (2) + 3 -->
 * input = 100 + 20 + 3 -->
 * input = 123
 *
 */