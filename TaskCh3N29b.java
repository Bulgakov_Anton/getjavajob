/*
 * Created by Anton on 15.06.2017.
 *
 * Записать условие, которое является истинным, когда:
 * б) только одно из чисел X и Y меньше 20; in b solve with <=2
 *
 * don’t use * / +  - operators, in b solve with <=2 and in d - with <=3 comparison operators;
 * don't use extra parentheses!
 * write task's text as javadoc for each solution;
 * д) task cover with all possible test inputs
 *
 */

public class TaskCh3N29b {
    public static void main (String[] args) {
        //  олько одно из чисел X и Y <= 2 (истина)
        int x = 13;
        int y = 2;
        boolean a = x % 10 <= 2;
        boolean b = y % 10 <= 2;
        boolean result = a ^ b ;
        System.out.println("result : " + result);
    }
}
