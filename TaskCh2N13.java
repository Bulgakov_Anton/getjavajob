/*
 * Created by Anton on 11.06.2017.
 *
 * Дано трехзначное число. Найти число, полученное при прочтении его цифр справа налево.
 *
 *
 */
public class TaskCh2N13 {
    public static void main(String args[]) {
        int input = 142;
        int result = 0;
        for (int cycle = 0; cycle < 3; cycle = cycle + 1)
        {
            result = result * 10;
            result = result + (input % 10);
            input = input / 10;
        }
        System.out.print(result);
    }
}

/*

​int input = 142;
int result = 0;

 -->

result = result * 10; --> 0 * 10 = 0
result = result + input % 10; --> 0 + 2(остаток от деления целого числа) = 2
input = input / 10; --> 142 / 10 = 14 (целое число после деления)

 -->

input == 14
result == 2

 -->

result = result * 10; --> 2 * 10 = 20
result = result + input % 10; --> 20 + 4(остаток от деления целого числа) = 24
input = input / 10; --> 14 / 10 = 1 (целое число после деления)

 -->

input == 1
result == 24

 -->

result = result * 10; --> 24 * 10 = 240
result = result + input % 10; --> 240 + 1(остаток от деления целого числа) = 241
input = input / 10; --> 1 / 10 = 0 (целое число после деления)


Цикл закончен, так как значение следующего цикла = 3 (условие cycle < 3), цикл увеличивается на 1 начиная с 0

​*/